from math import sqrt

from matplotlib import rcParams, pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch, Rectangle
from matplotlib.transforms import Affine2D as T

from svgpath2mpl import parse_path
import svgwrite

class Tile(object):
    
    def __init__(self, path = None):
        self.bbox = True
        if not path: path = Path([(0,0)])
        self.path = path

    @classmethod
    def read(cls, name):
        with open('{}.path'.format(name)) as _: 
            return cls(
                parse_path(_.read()).transformed(
                    T().scale(1, -1).translate(0, 1)
                )
            )
            
    @staticmethod
    def union(tile0, tile1):
        return Tile(Path.make_compound_path(tile0.path, tile1.path))
            
    @staticmethod
    def transform(tile, transform):
        return Tile(tile.path.transformed(transform))
    
    def svg_out(self):
        print("start print")
        fig, ax = plt.subplots()
        ax.add_patch(PathPatch(self.path, fill = False))
        if self.bbox:
            r = Rectangle((0,0), 1, 1, fill = False, linestyle = 'dotted')
            ax.add_patch(r)
        plt.axis('equal')
        plt.axis('off')
        plt.savefig("test4.svg")
        print("done!")


    def _ipython_display_(self):
        fig, ax = plt.subplots()
        ax.add_patch(PathPatch(self.path, fill = False))
        if self.bbox:
            r = Rectangle((0,0), 1, 1, fill = False, linestyle = 'dotted')
            ax.add_patch(r)
        plt.axis('equal')
        plt.axis('off')
        return fig

def flip(tile):
    return Tile.transform(tile, T().scale(-1, 1).translate(1, 0))

def rot(tile):
    return Tile.transform(tile, T().rotate_deg(90).translate(1, 0))

def rot45(tile):
    return Tile.transform(tile,
        T().rotate_deg(45).scale(1 / sqrt(2), 1 / sqrt(2)).translate(1 / 2, 1 / 2)
    )

def over(tile0, tile1):
    return Tile.union(tile0, tile1)

def beside(tile0, tile1, n = 1, m = 1):
    den = n + m
    return Tile.union(
        Tile.transform(tile0, T().scale(n / den, 1)),
        Tile.transform(tile1, T().scale(m / den, 1).translate(n / den, 0))
    )

def above(tile0, tile1, n = 1, m = 1):
    den = n + m
    return Tile.union(
        Tile.transform(tile0, T().scale(1, n / den).translate(0, m / den)),
        Tile.transform(tile1, T().scale(1, m / den))
    )

def quartet(p, q, r, s):
    return above(beside(p, q), beside(r, s))

def nonet(p, q, r, s, t, u, v, w, x):
    return above(
           beside(p, beside(q, r), 1, 2),
           above(
               beside(s, beside(t, u), 1, 2),
               beside(v, beside(w, x), 1, 2), 
           ),
           1, 2
    )

def side(n):
    if n == 0: 
        return blank
    else: 
        return quartet(side(n-1), side(n-1), rot(t), t)

def corner(n):
    if n == 0:
       return blank
    else:
        return quartet(corner(n-1), side(n-1), rot(side(n-1)), u)

def squarelimit(n):
    return nonet(
        corner(n), 
        side(n), 
        rot(rot(rot(corner(n)))), 
        rot(side(n)), 
        u, 
        rot(rot(rot(side(n)))),
        rot(corner(n)), 
        rot(rot(side(n))), 
        rot(rot(corner(n)))
     )

def _t(base):
    t2 = flip(rot45(base))
    t3 = rot(rot(rot(t2)))
    return over(base, over(t2, t3))

def _u(base):
    t2 = flip(rot45(base))
    return over(over(t2, rot(t2)), over(rot(rot(t2)), rot(rot(rot(t2)))))

blank = Tile()
f = Tile.read('f')
fish = Tile.read('fish')
edge = Tile.read('edge')
triangle = Tile.read('triangle')

#Change here between triangle and fish
t = _t(fish)
u = _u(fish)


if __name__== "__main__":
      print("Hello World!")
      #works!
      #quartet(flip(rot(rot(rot(f)))), rot(rot(rot(f))), rot(f), flip(rot(f))).svg_out()
      #next 
      rcParams['figure.figsize'] = 1, 1
      fish.svg_out()
